function H = spectrum(h)
% SPECTRUM Compute the spectrum of a given filter.
%   H = SPECTRUM(h) computes the frequency spectrum H of a given (wavelet)
%   filter h.

% Define a discrete frequency grid
w = -pi:(pi/1000):pi;
H = zeros(1, length(w));
N = length(h);

% And compute the Discrete Fourier Transform with the well-know
% formula.
for i = 1:length(w)
    k = 0:1:(N-1);

    H(i) = dot(h, exp(1i*w(i)*k));
end
end