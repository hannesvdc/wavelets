function s = support(signal)
% SUPPORT Find the support of a signal.
%   s = SUPPORT(signal) returns the support (the first to last nonzero 
%   sample) of a given signal.

% find all indices that are nonzero
indices = find(signal ~= 0);

% return the maximal and minimal coefficient, this forms the discrete 
% support.
s = [min(indices), max(indices)];
end