function varargout = plotBold(signal, fs, varargin)
% PLOTBMS Plot a biomedical signal.
%   PLOTBMS(signal, fs) plots the given BOLD signal with given
%   sampling frequency fs. The plot has standard title 'BOLD signal', 
%   line specification '-' and linewidth 1. The time axis
%   starts from 0 seconds and the ylabel is 'Blood oxygen-level'.
%   Note: the function doesn't make a new figure.
%
%   PLOTBMS(signal,fs,S) plots the biomedical signal with extra arguments S, 
%   containing following possible combinations of settings:
%       'Title',titlep: the title of the plot
%       'Linespec',linespec: the line specification 
%       'Linewidth',linewidth: the linewidth 
%       'BeginSample',bs: when plotting a segment, this is the first
%                         sample of the original signal.
%       'ylabel',yl: the ylabel of the plot
%       'color',c: the color of the plot
%   When an argument is not given, above standard value is taken.

%
%   h = PLOTBMS(...) returns the function handle with the 
%   specified or standard settings (see above). 

%% Check arguments
if mod(nargin,2) == 1
    error('Invalid argument list.');
end
titlep = 'BOLD signal';
linespec = '-';
linewidth = 1;
bs = 1;
yl = 'Blood oxygen level';
c = [0,102/255,204/255];
for k = 1:length(varargin)/2
   if strcmp(varargin{2*k-1},'Title')
       titlep = varargin{2*k};
   elseif strcmp(varargin{2*k-1},'Linespec')
       linespec = varargin{2*k};
   elseif strcmp(varargin{2*k-1},'Linewidth')
       linewidth = varargin{2*k};
   elseif strcmp(varargin{2*k-1},'BeginSample')
       bs = varargin{2*k};
   elseif strcmp(varargin{2*k-1},'ylabel')
       yl = varargin{2*k};    
   elseif strcmp(varargin{2*k-1},'color')
       c = varargin{2*k};           
   else
       error('Invalid argument list.');
   end
end

%% Plot
slen = length(signal);
t = (bs:slen+bs-1)/fs;
varargout{1} = plot(t,signal,linespec,'Linewidth',linewidth,'color',c);
xlabel('Time [s]','Interpreter','Latex');
ylabel(yl,'Interpreter','Latex');
title(titlep,'Interpreter','Latex');
axis tight;

end