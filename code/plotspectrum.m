function plotspectrum(h)
% PLOTSPECTRUM Plot the spectrum of a filter.
%   PLOTSPECTRUM(h) plots the spectrum of given filter h, which is the
%   output of 'spectrum'.

H = spectrum(h);
N = length(H);
plot(linspace(-pi, pi, N), 20*log10(abs(H)), 'LineWidth', 2);
xlabel('Normalized frequency','Interpreter','Latex');
ylabel('Amplitude spectrum [dB]','Interpreter','Latex');
end