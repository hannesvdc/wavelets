function freqIntervals = waveletbands(fs,L)
% WAVELETBANDS Compute the division of the wavelet bands in the frequency
% domain.
%   freqIntervals = WAVELETBANDS(fs,L) computes and plots the division of
%   the frequency domain, based on sampling frequency fs, in to the
%   different wavelet bands, corresponding to L leves. freqIntervals is a
%   2 x L+1 matrix with the first row the begin indices of each interval
%   and the second row the end indices. The first L columns correspond to
%   the wavelet spaces W_j, the last one with scaling space V_{J-L}.

%% Compute the intervals
freqIntervals = zeros(2,L+1);
for j = 1:L
   freqIntervals(1,j) = fs/(2^(j+1));
   freqIntervals(2,j) = fs/(2^j);
end
freqIntervals(1,end) = 0;
freqIntervals(2,end) = fs/(2^(L+1));

%% Plot the division
figure;
hold on;
for k = 1:L+1
   plot([freqIntervals(1,k) freqIntervals(2,k)],[0 0],'k-','Linewidth',1.5);
   plot(freqIntervals(1,k)*ones(2,1),[-0.3 0.3],'b-','Linewidth',1.5);
   if k < L+1
       text(mean(freqIntervals(:,k)),0.4,['$W_{J-',num2str(k),'}$'],'Color','r','FontWeight','bold','Interpreter','Latex');
   else
       text(mean(freqIntervals(:,k)),0.4,['$V_{J-',num2str(L),'}$'],'Color','r','FontWeight','bold','Interpreter','Latex');
   end
end
plot(freqIntervals(2,1)*ones(2,1),[-0.3 0.3],'b-','Linewidth',1.5);
plot([freqIntervals(2,1),freqIntervals(1,:)],zeros(length(freqIntervals(1,:))+1,1),'bo','Linewidth',2);
xlabel('$f$ [Hz]','Interpreter','Latex');
ylabel('Nothing meaningful','Interpreter','Latex');
title('Division of the frequency domain','Interpreter','Latex');
set(gca,'YTickLabel',[]);
axis([0 fs/2 -1 1]);
end