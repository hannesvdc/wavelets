%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% BOLD analysis
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load and define data
clear;
close all;
load bold;
fs = 0.5;
orange = [0.9100, 0.4100, 0.1700] ;       % Color specification

%% Plot data
figure;
plotBold(bold,fs);

%% Define parameter wavelet transform
L = 5;
wname = 'db4';

%% Compute sJ0
[C,l] = wavedec(bold,L,wname);
[phi,maxI] = evalscalingfunction(bold,L,wname,1,1);
s = zeros(length(phi),1);
for j = 1:maxI
   s = s + C(j).*evalscalingfunction(bold,L,wname,1,j); 
end

%% Compute wJk

w = zeros(length(phi),L);
J = floor(log2(length(bold)));
for level = J-L:J-1
   [~,maxI] = evalwavelet(bold,L,wname,level,1);
   for j = 1:maxI
       w(:,level+L-J+1) = w(:,level+L-J+1) + C(sum(l(1:(level+1-(J-L))))+j).*evalwavelet(bold,L,wname,level,j);
   end
end

%% Plotting
disp(waveletbands(fs,L));

figure;
subplot(2,ceil((L+1)/2),1);
plotBold(s,fs,'Title',['Scaling approximation on coarsest scale J-',num2str(L)]);
for k = 1:L
    subplot(2,ceil((L+1)/2),k+1);
    plotBold(w(:,k),fs,'Title',['Wavelet approximation on level J-',num2str(L-k+1)]);
end

figure;
plotBold(s+sum(w,2),fs);
hold on;
plotBold(bold,fs,'color',orange,'Linespec','--');
legend('Original BOLD signal','BOLD signal after reconstruction');

