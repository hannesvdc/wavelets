function varargout = evalwavelet(s, L, wname, level, index)
% EVALWAVELET Compute a wavelet for on a given scale and time index.
%   psi = EVALWAVELET(s,L,wname,level,index) computes a wavelet wname, 
%   given a signal s with length N, which is wavelet transformed on L 
%   levels, on level level and time index index.
%
%   [psi,K] = EVALWAVELET(s,L,wname,level,index) computes a wavelet and
%   returns the number of wavelet functions K on that level.

%% Check arguments
% bookkeeping parameter
J = floor(log2(length(s)));
if level < J-L || level > J-1
    error('Unvalid level.');
end

%% Compute wavelet function
% compute the wavelet transform of s
[c, l] = wavedec(s, L, wname);

% check index
if index < 1 || index > l(end-(J-level))
    error('Unvalid index.');
end

% put c to zero and set the right coefficients to one
c = zeros(size(c));
J = floor(log2(length(s)));
c(sum(l(1:(level+1-(J-L))))+index) = 1;

% and return the inverse wavelet transform
psi = waverec(c, l, wname);
varargout{1} = psi;
if nargout > 1
    varargout{2} = l(end-(J-level));
end
end