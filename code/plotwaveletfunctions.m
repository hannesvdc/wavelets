%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Plot wavelet functions
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s = rand([1, 128]);
wname = 'db4';
L = 4;

figure();
for index = 1:2:12
   psi = evalwavelet(s, L, wname, 3, index);
   plot(psi, '-', 'LineWidth', 2); hold on;
end
title('Wavelet functions on level 6');

level = floor(log2(length(s)));

pp = evalwavelet(s,L,wname,3,3).*evalwavelet(s,L,wname,3,7);
figure
plot(pp)
trapz(pp)