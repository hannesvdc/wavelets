function varargout = evalscalingfunction(s, L, wname, level, index)
% EVALSCALINGFUNCTION Compute a wavelet for on a given scale and time index.
%   phi = EVALSCALINGFUNCTION(s,L,wname,level,index) computes a scaling 
%   function wname, given a signal s with length N, which is wavelet 
%   transformed on L levels, on level level and time index index.
%
%   [phi,K] = EVALSCALINGFUNCTION(s,L,wname,level,index) computes a scaling
%   function and returns the number of wavelet functions K on that level.

%% compute the discrete wavelet transformation
[c, l] = wavedec(s, L, wname);

% put all coefficients to zero except the one at index
c = zeros(size(c));
c(index) = 1;

% and return the scaling function as the inverse of the modified
% series.
phi = waverec(c, l, wname);
varargout{1} = phi;
if nargout > 1
    varargout{2} = l(1);
end
end