%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Pressure data analysis
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load and plot
load pressure;
close all;
t=0:1:(length(pressure)-1);
t = t*(1./3000);
plot(t, pressure);
fs = 3000;

%% Compute wavelet transform
L = 16;
[c, l] = wavedec(pressure, L, 'db2');
interval = [20*fs, 35*fs];

%% Plot the wavelet frequency decomposition
figure;
xas = linspace(1, fs/2, length(c));
semilogy(xas, abs(c)); hold on;
for i = 1:1:L
    freq = fs/2^i;
    plot(freq*ones(2,1),[10 1],'r-','Linewidth',1.5); hold on;
end

% Compute all the coefficients that overlap with the interval [20,35] and
% put them in a cell per level of the wavelet transform.
overlapcoefficients = cell(L+1, 1);
scalesupport = [];
for k = 1:l(1)
    phi = evalscalingfunction(pressure, L, 'db4', 0, k);
    sup = support(phi);
    
    % check if the finite support of the scaling function overlaps with 
    % time interval [20, 35].
    if ((sup(2) >= interval(1)) && (sup(1) <= interval(2))) || ...
        ((sup(2) >= interval(1)) && (sup(1) <= interval(2)))
        scalesupport = [scalesupport, c(k)];
    end
    overlapcoefficients{1} = scalesupport;
end

% And do the same for all levels of the wavelet transform.
J = floor(log2(length(pressure)));
for level = (J-L):(J-1)
    disp(['Wavelet level ', num2str(level)]);
    waveletsupport = zeros(1, l(level - (J-L)+2));
    offset = sum(l(1:(level-(J-L)+1)));
    
    for k = 1:l(level - (J-L) + 2)
        psi = evalwavelet(pressure, L, 'db4', level, k);
        sup = support(psi);
        
        % check if the finite support of the wavelet overlaps with time 
        % interval [20, 35].
        if ((sup(2) >= interval(1)) && (sup(1) <= interval(2))) || ...
            ((sup(2) >= interval(1)) && (sup(1) <= interval(2)))
            waveletsupport(k) = c(offset + k);
        end
    end
    
    overlapcoefficients{level - (J-L) + 2} = waveletsupport;
end