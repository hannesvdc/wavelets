%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Plot scaling function
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define wavelet (parameters)
s = rand([1, 128]);
wname = 'db4';
L = 4;

%% Plot scaling functions
figure;
for index = 1:2:16
   phi = evalscalingfunction(s, L, wname, 1, index);
   plot(phi, '-', 'LineWidth', 2); hold on;
end
title('Scaling functions');

level = floor(log2(length(s)));

