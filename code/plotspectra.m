%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Plot spectra decomposition and reconstruction filters
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

wnames = {'db2','db4'};
for k = 1:length(wnames)
    [LO_D,HI_D,LO_R,HI_R] = wfilters(wnames{k});
    figure;
    subplot(1,2,1);
    plotspectrum(LO_D)
    hold on;
    plotspectrum(HI_D)
    legend('Low pass decomposition','High pass decomposition')
    axis tight;
    title(['Decomposition filters ',wnames{k}],'Interpreter','Latex');
    
    subplot(1,2,2);
    plotspectrum(LO_R)
    hold on;
    plotspectrum(HI_R)
    legend('Low pass reconstruction','High pass reconstruction')
    axis tight;
    title(['Reconstruction filters ',wnames{k}],'Interpreter','Latex');  
end