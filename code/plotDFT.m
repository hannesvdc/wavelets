%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Plot DFT's of wavelets
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fs = 1000;

%% Define wavelet
N = 2^10;
L = 10;
level = 6;
index = 20;
wname = 'coif3';
psi = evalwavelet(randn(1,N),L,wname,level,index);
figure(1);
hold on;
plot(psi,'Linewidth',2)
xlabel('Time (samples)','Interpreter','Latex');
ylabel('Amplitude','Interpreter','Latex');
axis tight;

%% Compute DFT and make plot
figure(2);
hold on;
DFT = fftSignal(psi,fs);
axis tight;