function DFT = fftSignal(signal,fs)
% FFTSIGNAL Compute and plot the DFT of given signal with sampling
% frequency fs.
%   DFT = FFTSIGNAL(signal,fs) computes and plots the normalized DFT of the
%   given signal with sampling fs.

%% Compute DFT
DFT = fft(signal,2^(ceil(log2(length(signal))+5))); % Take more points in the FFT to increase smoothness

%% Define frequency range
flen = length(DFT);
ff = fix(flen/2) + 1;

%% Compute spectrum and normalize
magn = abs(DFT);
normaln = max(magn);
f = (1:ff)*fs/flen;
magnSpec = 20*log10(magn/normaln);


%% Plot spectrum
plot(f,magnSpec(1:ff),'-','Linewidth',1.5);
xlabel('$f$ [Hz]','Interpreter','Latex');
ylabel('Amplitude spectrum [dB]','Interpreter','Latex');
title('Modulus DFT','Interpreter','Latex');
axis tight;

end