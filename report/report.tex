\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{shapes,arrows}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{float}
\usepackage{listings}
\usepackage{epstopdf}
\usepackage{amsfonts}

\renewcommand\floatpagefraction{.9}
\renewcommand\dblfloatpagefraction{.9} % for two column documents
\renewcommand\topfraction{.9}
\renewcommand\dbltopfraction{.9} % for two column documents
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1}   
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}

\date{Academic year 2017-2018}
\address{
	Master Mathematical Engineering \\
	Wavelets with application in Signal and Image Processing \\
	Prof. Dr. Ir. Daan Huybrechs}
\title{Data analysis using wavelets}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
	
	\lstset{language=Matlab,%
		%basicstyle=\color{red},
		breaklines=true,%
		linewidth=14cm,
		basicstyle=\scriptsize,
		morekeywords={matlab2tikz},
		keywordstyle=\color{blue},%
		morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
		identifierstyle=\color{black},%
		stringstyle=\color{mylilas},
		commentstyle=\color{mygreen},%
		showstringspaces=false,%without this there will be a symbol in the places where there is a space
		numbers=left,%
		numberstyle={\tiny \color{black}},% size of the numbers
		numbersep=9pt, % this defines how far the numbers are from the text
		emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
		%emph=[2]{word1,word2}, emphstyle=[2]{style},    
	}
	
	\maketitle
	
	\tableofcontents
	
	\section{Time- and frequency localization of wavelets using data}
	
	In this section, we investigate the different time- and frequency localization properties of wavelets, in the light of the analysis of signals. In signal processing, signals $s(t)$ are sampled with a sampling frequency $f_s$. The Nyquist theorem learns us that the maximal frequency of the continuous-time signal should have been $\frac{f_s}{2}$, otherwise \emph{aliasing} occurs. $\frac{f_s}{2}$ is the maximal frequency we can represent.

	
	\subsection{The frequency intervals of wavelet scales}
	
	The discrete wavelet decomposition can be interpreted as a change of basis, where we start from space $V_J$ on the finest scale and recursively split the scaling spaces in the direct sum of a new scaling and wavelet space on a coarser scale. After $L$ steps, we have:
	\begin{equation} \label{eq:functionspacedecomposition}
		V_J = V_{J-L}  \oplus W_{J-L} \oplus W_{J-L+1} \oplus \dots \oplus W_{J-1}.
	\end{equation}
	
	More concrete, it corresponds to applying a filter bank where in each step, a high-pass and low-pass system filter the signal (theoretically the scaling coefficients on the finest scale) and down sample it. The ideal low-pass and high-pass filter split the spectrum into two equal parts of half the bandwidth. After one step, this means that the bandwidth of both signals left is $\frac{f_s}{4}$, which means that, according to the Nyquist theorem, we can also down sample the signal to sampling rate $\frac{f_s}{2}$, without have the risk of aliasing. Figure \ref{fig:ex12_filterbank} shows the filter bank with corresponding spaces and bandwidths of the outgoing signals. 
	
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}[auto,>=latex']
		\tikzstyle{block} = [draw, shape=rectangle, minimum height=3em, minimum width=3em, node distance=2cm, line width=1pt]
		\tikzstyle{ds} = [draw, shape=circle, node distance=1.5cm, line width=1pt, minimum width=1.25em]
		\tikzstyle{branch}=[fill,shape=circle,minimum size=2pt,inner sep=0pt]
		%Creating Blocks and Connection Nodes
		\node at (-2.5,0) (input) {$V_J, \frac{f_s}{2}$};
		\node [block] (hp1) {HP-filter};
		\node at (9.5,0) (output1) {$W_{J-1}, \frac{f_s}{4}$};
		\node [ds, right of = hp1] (ds1) {$\downarrow 2$};
		\path (hp1) -- coordinate (med) (ds1);
		\path (input) -- coordinate(branch1) (hp1);
		\node [block, below of=hp1] (lp1) {LP-filter};
		\node [ds, right of = lp1] (ds2) {$\downarrow 2$};
		\node [block, right of = ds2] (hp2) {HP-filter};
		\node at (1.6,-2) (tak1) {};
		\path (tak1) -- coordinate(branch2) (hp2);
		\node [block, below of=hp2] (lp2) {LP-filter};
		\node at (9.5,-2) (output2) {$W_{J-2}, \frac{f_s}{8}$};
		\node [ds, right of = hp2] (ds3) {$\downarrow 2$};
		\node [ds, right of = lp2] (ds4) {$\downarrow 2$};
		\node at (9.5,-4) (ppp) {\dots};
		%Conecting Blocks
		\begin{scope}[line width=1pt]
		\draw[->] (input) -- (hp1);
		\draw[->] (hp1) -- (ds1);
		\draw[->] (ds1) -- (output1);
		\draw[->] (branch1) node[branch] {} |- (lp1);
		\draw[->] (lp1) -- (ds2);
		\draw[->] (ds2) -- (hp2);
		\draw[->] (branch2) node[branch] {} |- (lp2);
		\draw[->] (ds3) -- (output2);
		\draw[->] (hp2) -- (ds3);
		\draw[->] (lp2) -- (ds4);
		\draw[->] (ds4) -- (ppp);		
		\end{scope}
		\end{tikzpicture}
		\caption{The wavelet filter bank, with corresponding spaces, applied to a signal, sampled at $f_s$. The corresponding bandwiths of the signals is also indicated.}
		\label{fig:ex12_filterbank}
	\end{figure}

		Lets now consider a signal $s(t)$ with a sampling frequency $f_s = \SI{1000}{\hertz}$. Taking the number of levels $L = 5$, Figure \ref{fig:ex12_freqDiv} and Table \ref{tab:ex12_freqDiv} show the division of the relevant frequency range $[0,\frac{f_s}{2}]$. The plot and values are found with the \textmd{MATLAB} function \texttt{waveletbands}.
	
	\begin{table}
		\centering
		\begin{tabular}{@{} l|c|c @{}}    
			\toprule
			\emph{Space} & \emph{Frequency interval} & \emph{Bandwidth} \\
			\midrule
			$W_{J-1}$ & $[\SI{250}{\hertz},\SI{500}{\hertz}]$ & $\SI{250}{\hertz}$\\
			$W_{J-2}$ & $[\SI{125}{\hertz},\SI{250}{\hertz}]$ & $\SI{125}{\hertz}$\\
			$W_{J-3}$ & $[\SI{62.5}{\hertz},\SI{125}{\hertz}]$ & $\SI{62.5}{\hertz}$\\
			$W_{J-4}$ & $[\SI{31.25}{\hertz},\SI{62.5}{\hertz}]$ & $\SI{31.25}{\hertz}$\\
			$W_{J-5}$ & $[\SI{15.625}{\hertz},\SI{31.25}{\hertz}]$ & $\SI{16.625}{\hertz}$\\
			$V_{J-5}$ & $[\SI{0}{\hertz},\SI{15.6250}{\hertz}]$ & $\SI{16.625}{\hertz}$\\
			\bottomrule			
			\hline
		\end{tabular}
		\caption{The different frequency intervals for $f_s = \SI{1000}{\hertz}$ and $L = 5$.}
		\label{tab:ex12_freqDiv}
	\end{table}
	
	\begin{figure}
		\centering
		\includegraphics[scale = 0.4]{plots/freqDiv12}
		\caption{The division of the frequency domain in wavelet spaces for $f_s = \SI{1000}{\hertz}$ and $L = 5$.}
		\label{fig:ex12_freqDiv}
	\end{figure}

	Note that from a theoretical viewpoint, this frequency division is independent from the choice of the wavelet. However, in practice, these low- and high-pass filter are not ideal in the sense that they take for example for a low-pass filter, also information from the high-pass band. The filtering is not perfect, such that down sampling results in aliasing as the highest frequency will be higher than the half of the sampling frequency. This is also shown in Section \ref{sec:freq_loc_rev}.
	
	\subsection{Time localization}
	
	Wavelets can be used for time as well as frequency localization due to the inherent two-scale relation. Every function $f \in V_J$ can be decomposed in an expansion like
	\begin{equation} \label{eq:functiondecomposition}
	f(x) = \sum_{k \in \mathbb{Z}} v_{J_0, k} \phi_{J_0,k}(x) + \sum_{j = J_0}^{J-1} \sum_{k \in \mathbb{Z}} w_{j,k} \psi_{j,k}(x),
	\end{equation}
	where the scaling functions $\phi_{J_0,k}(x)$ live in the function space $V_{J_0}$ and the wavelet functions $\psi_{j,k}(x)$ in $W_j$. This is exactly the same function space decomposition as (\ref{eq:functionspacedecomposition}) but then written in terms of functions instead of abstract function spaces. The parameter $J_0$ is somewhat arbitrary and equals $J-L$ from the previous section. This determines how coarse in time or fine in frequency the decomposition needs to be. A proper choice is application specific.
	
	\vspace{2mm}
	\noindent
	The basis functions $\phi_{J_0,k}(x)$ and $\psi_{j,k}(x)$ are of course defined by the wavelet type but can be visualized (in discrete points) by putting all scaling and wavelet coefficients in (\ref{eq:functiondecomposition}) to zero except the one corresponding to the wavelet or scaling function that we want to visualize. Then applying the inverse wavelet transform should the return this sampled wavelet or scaling function. This is implemented in the functions \texttt{evalscalingfunction} and \texttt{evalwavelet}. The functions return the scaling or wavelet function sampled in the same points as the input sequence. To illustrate this, let's apply these functions to a random signal of length 128 ($J=7$) and compute $L=4$ levels of the wavelet transform (\ref{eq:functiondecomposition}) (hence $J_0=3$). Figure \ref{fig:wavelet6} shows a few translated wavelet functions on level 6 for the Daubechies-2 wavelets, Figure \ref{fig:wavelet3} the wavelet functions on level 3 and Figure \ref{fig:scaling3} the scaling functions on the same level. It is indeed the case that on the same level the wavelet (or scaling) functions are just translations of each other and wavelets on a coarser scale are wider (in this case $16=2^4$ times) than their finer counterparts. For completeness Figures \ref{fig:waveletdb43}, \ref{fig:waveletdb46} and \ref{fig:scalingdb4} show the wavelet and scaling functions on the same levels for the Daubechies-4 wavelet family. These seem to have a longer support and they will have a longer FIR-sequence too (due to the higher vanishing moments, more conditions are put on the coefficients, such that the support is longer).
	
	There is also another helper function \texttt{support} that computes the support of a given scaling function or wavelet. Almost all wavelets have a finite support to have a decent localization in time. This function returns the minimal and maximal index where the given wavelet or scaling function is non zero. As an example, the support of the yellow scaling function in Figure \ref{fig:scaling3} is [35, 80] and of the light blue wavelet in Figure \ref{fig:wavelet6} [99, 102]. Also the support of the purple wavelet on the coarsest level is [67, 112]. Normally the latter should have a continuous support that is eight times larger than that of the blue wavelet, but this isn't necessarily the case with the discrete support as in this example. The continuous support could stretch a little bit beyond the discrete support so that a factor of 2 per level does not need to hold in the discrete context.
	\begin{figure}
		\centering
		
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/wavelet6}
		\caption{A few different wavelet functions on the sixth level. There should be 65 of these. This is the first step in decomposition (\ref{eq:functiondecomposition}).}
		\label{fig:wavelet6}
\end{subfigure}

\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/wavelet3}
		\caption{A few different wavelet functions on the coarsest level $W_{J_0}$. In total there are 10 of these.}
		\label{fig:wavelet3}
\end{subfigure}

\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/scaling3}
		\caption{5 different scaling functions for a signal of length 128 with a 4-level wavelet transform. There should be ten of these scaling functions but only five were plotted.}
		\label{fig:scaling3}
\end{subfigure}
\caption{A few scaling and wavelet functions for the Daubechies-2 wavelets.}
	\end{figure}

	\begin{figure}
	\centering
	\begin{subfigure}{0.55\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletdb46}
		\caption{A few different wavelet functions on the sixth level.}
		\label{fig:waveletdb46}
	\end{subfigure}

	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletdb43}
		\caption{A few different wavelet functions on the coarsest level $W_{J_0}$.}
		\label{fig:waveletdb43}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/scalingdb4}
		\caption{5 different scaling functions for a signal of length 128 with a 4-level wavelet transform.}
		\label{fig:scalingdb4}
	\end{subfigure}
	\caption{A few scaling and wavelet functions for the Daubechies-4 wavelets.}
\end{figure}	
	\subsection{Frequency localization, revisited}
	\label{sec:freq_loc_rev}
	The wavelets described above have an exact finite support in the time domain, hence their Fourier transform will have an infinite support, or said differently they will only have an approximate frequency localization. Depending on the type of wavelet, this localization will be better or worse in the frequency domain. We can visually plot the frequency response to see how fast the edges of the high- and low-pass finite impulse response filters corresponding to the wavelets and scaling functions drop to zero in the frequency domain. This can easily be achieved by evaluating the transfer function $H(\omega) = \sum_k h_k e^ {i \omega k}$ in a discrete set of $\omega$'s and plotting $|H(\omega)|$. For example, the low- and high-pass filters corresponding to a Daubechies-2 wavelet are shown in Figure \ref{fig:filtersdb2}. For completeness the low- and high-pass filters for the higher order Daubechies-4 wavelet are shown in Figure \ref{fig:filtersdb4}. This resembles the ideal low- and high-pass filters better since they go faster to zero outside their active frequency region. Note that in the case of the Daubechies wavelet family, the filters on the analysis and synthesis side are the same, because it is a family of orthogonal wavelets.
	
	\noindent
	The filters in the figures are definitely not ideal high- or low-pass filters so aliasing will always occur in both arms of the filter bank after decomposition. There is however the property of perfect reconstruction so after the reconstruction step the signal should exactly equal the input signal even if aliasing occurs somewhere within the filter bank. Higher order filters will have edges that fall faster so aliasing will happen less with those wavelets.
\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{plots/filtersdb2}
	\caption{The low- and high-pass filter of the filter bank corresponding to the Daubechies-2 wavelets and scaling functions}
	\label{fig:filtersdb2}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{plots/filtersdb4}
	\caption{The low- and high-pass filter of the filter bank corresponding to the Daubechies-4 wavelets and scaling functions}
	\label{fig:filtersdb4}
\end{figure}

We can however also examine the frequency localization of the wavelets by looking at the FFT of the wavelet itself. We compare three elements: the Daubechies-2 wavelet on two different scales; the Haar, Daubechies-2 and Daubechies-4 wavelet and the Daubechies-3 and Coiflet-3 wavelet. Note that all the plots were generated with $f_s = \SI{1000}{\hertz}$, for a signal of length $2^{10} (J = 10)$. If it is not mentioned, the wavelet transform was taken on level 6, although not relevant, because we compare different wavelets on the same scale, if not mentioned otherwise.
\begin{itemize}
	\item We compare the Daubechies-2 wavelet on scale 5 and 6 (with $J = 10$). The results are shown in Figure \ref{fig:waveletDFTDBSCALE}. Figure \ref{fig:waveletDBTIMESCALE} shows the two wavelets in time. On the finer scale, we indeed see that the support is half as big. We expect that in the frequency domain, this results in a wider frequency localization (a wider support), because of the time-frequency duality. Figure \ref{fig:waveletDFTDBSCALE} confirms this: the main lobe is contains a double frequency range. 
	\item We also want to compare different wavelets from the Daubechies family. We expect that the higher the number of vanishing moments, the wider the time support becomes (because more constraints are put on the wavelet function) and thus finer the frequency localization. We compare here the Haar (= Daubechies-1), Daubechies-2 and Daubechies-4 wavelet. Figure \ref{fig:waveletDB} supports our hypothesis. Increasing the order results in a wider time support (Figure \ref{fig:waveletDBS}) and thus in a smaller main lobe, as is shown in Figures \ref{fig:waveletDFTDB} and \ref{fig:waveletDFTDBZoom}. Figure \ref{fig:waveletDFTDBZoomZoom} shows that also the center frequency of this main lobe decreases a bit.
	\item Feeding the samples of a signal instead of its scaling coefficients, is known as the \emph{wavelet crime}. Coiflets try to avoid this crime. This results in a much faster convergence from samples to the scaling coefficients. However, \emph{there is no such thing as a free lunch}: the cost is a wider time support because of the more restricted wavelet. This should however result in a finer main lobe of the frequency spectrum. Figure \ref{fig:waveletCOIF} confirms our expectations. Note that Figure \ref{fig:waveletCOIFS} shows the full support of the Coiflet wavelet, although it is almost zero at the edges. Using the \texttt{support} function, we find that the support of the Daubechies-3 wavelet is 76 samples, while the Coiflet wavelet has a support of 256 samples. 
\end{itemize}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDB2SCALE}
		\caption{Time signals}
		\label{fig:waveletDBTIMESCALE}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDFTDB2SCALE}
		\caption{Normalized frequency spectrum}
		\label{fig:waveletDFTDBSCALE}
	\end{subfigure}
	\caption{Comparison of the Daubechies-2 wavelet on scale 5 and 6}
	\label{fig:waveletDBSCALE}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDBS}
		\caption{Time signals}
		\label{fig:waveletDBS}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDFTDB}
		\caption{Normalized frequency spectrum}
		\label{fig:waveletDFTDB}
	\end{subfigure}

	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDFTDBZoom}
		\caption{A zoom of the normalized frequency spectrum on the main lobe}
		\label{fig:waveletDFTDBZoom}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDFTDBZoomZoom}
		\caption{A zoom of the ormalized frequency spectrum on the maximal frequency}
		\label{fig:waveletDFTDBZoomZoom}
	\end{subfigure}
	\caption{Comparison of three wavelets from the Daubechies family}
	\label{fig:waveletDB}
\end{figure}	

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletCOIFS}
		\caption{Time signals}
		\label{fig:waveletCOIFS}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=1\linewidth]{plots/waveletDFTCOIF}
		\caption{Normalized frequency spectrum}
		\label{fig:waveletDFTCOIF}
	\end{subfigure}
	\caption{Comparison of the Daubechies-3 wavelet with the Coiflet-3 wavelet}
	\label{fig:waveletCOIF}
\end{figure}

	\section{Analysis of two data sets}
	In this section, we apply the discrete wavelet transform, with its interesting time- and frequency localization properties, to two real-life data sets: a BOLD set and a pressure plate data set.
	
	\subsection{Blood oxygen-level dependent imaging (BOLD)}
	Figure \ref{fig:ex22_bold} shows a BOLD signal (which is very noisy, we're getting used to it \dots), taken at a specific voxel in the brain, sampled at $f_s = \SI{0.5}{\hertz}$. The data set given has a length of 768 samples, so the length of the trial is equal to $\frac{768 \text{ samples}}{0.5 \frac{\text{samples}}{\text{s}}} = \SI{1536}{\second} = \SI{25}{\minute} \text{ } \SI{36}{\second}$, as is indicated in the plot. Note that the heart rate of the subject can be viewed as high-frequency noise here, because the mean heart rate of a person is about $60-70 \frac{\text{ beats}}{\text{ s}}$, which is very high-frequent for a signal where the relevant information is in the $10-\SI{100}{\milli\hertz}$.
	
	\begin{figure}
		\centering
		\includegraphics[width = 1\linewidth]{plots/bold}
		\caption{The BOLD signal}
		\label{fig:ex22_bold}
	\end{figure}
	
	Given that the signal is non-stationary, wavelets are a better option to analyze frequency content of the signal, given that we know something about the time localization of the wavelets.  This is the problem with the Fourier analysis: the basis functions are sine and cosine functions, which are perfectly localized in frequency, but have an infinite support, such that there is no time information available. 
	
	To analyze the time-frequency properties of the BOLD signal, we decompose the signal into a superposition of signals, corresponding to the different wavelet scales as before. Defining again $J = \lfloor\log_2(N)\rfloor = 9$, with $N = 768$ and choosing $L = 5$ levels, we have the following decomposition (also see Equation \ref{eq:functiondecomposition}):
	\[
		f(t) = s_{J-L}(t) + w_{J-L}(t) + w_{J-L+1}(t) + \dots + w_{J-1}(x),
	\]
	
	with 
	\[
		s_{J-L}(t) = \sum_{k}^{}v_{J-L,k}\phi_{J-L,k}(t) \text{ and } w_{J-L+m}(t) = \sum_{k}^{}w_{J-L+m,k}\psi_{J-L+m,k}(t), \forall m \in {0,1,\dots,L-1}.
	\]
	
	Each of these signals live in a certain frequency range, corresponding to the different levels of the filter bank. We can thus select those component signals that live in the range $[10,100]$ $\SI{}{ \milli\hertz}$  to analyze the BOLD signal.
	
	As a wavelet, we chose the Daubechies-4 wavelet, because it is a smooth wavelet function, such that the representation of the different corresponding signals is also visually smooth. It has a relatively compact support and it has only one large positive peak, such that it is very similar to the peaky form of the BOLD signal. This wavelet for $L = 5$, $k = 10$, with $f_s = \SI{0.5}{\hertz}$ is shown in Figure \ref{fig:ex22_db4wavelet5}, found using \texttt{evalwavelet}.
	
	\begin{figure}
		\centering
		\includegraphics[scale = 0.4]{plots/db4wavelet5}
		\caption{The Daubechies-4 wavelet for $L = 5, k = 10$ and $f_s = \SI{0.5}{\hertz}$}
		\label{fig:ex22_db4wavelet5}
	\end{figure}
	
	 The number of levels $L$ in the wavelet decomposition is chosen to be 5. We motivate this choice using Figure \ref{fig:ex22_freqDiv}: the figure shows that if we would use more than 5 levels, we would further divide the subband of $[0,7.8]$ $\SI{}{\milli\hertz}$, corresponding to $V_{J-5}$, where there is no relevant clinical information. Using less than 5 levels results in splitting $[10,100]$ $\SI{}{\milli\hertz}$ in less subintervals, which means that the corresponding signal(s) contain more aggregate, and thus potential less, information. The optimal value is thus 5. 
	
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{plots/freqDiv22}
		\caption{The division of the frequency domain for $f_s = \SI{0.5}{\hertz}$ and $L = 5$}
		\label{fig:ex22_freqDiv}
	\end{figure}

	Figure \ref{fig:ex22_waveletDecomp} shows the result of this decomposition. We indeed see that higher levels correspond to more high-frequent information.
	
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{plots/waveletDecomp22}
		\caption{The decomposition of the BOLD signal using 5 levels}
		\label{fig:ex22_waveletDecomp}
	\end{figure}

	Using Figure \ref{fig:ex22_freqDiv}, we see that the signals $w_{J-5}(t),w_{J-4}(t), w_{J-3}(t)$ and $w_{J-2}(t)$ contain frequency information in the relevant range $[10,100]$ $\SI{}{\milli\hertz}$. These signals can thus be used for further investigation.
	
	Note that we implemented this decomposition using the function \texttt{evalwavelet} and \texttt{evalscalingfunction}, although we also could have used \texttt{waverec}. However, this allows to verify the code by reassembling the different signals and comparing this to the original signal. Figure \ref{fig:ex22_boldReconstr} shows that the reconstruction indeed succeeded and that the code is correct.
	
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{plots/boldReconstr}
		\caption{The original and reconstructed BOLD signal}
		\label{fig:ex22_boldReconstr}
	\end{figure}
	
	\subsection{Human pressure response}
	Another application of wavelets in the biomedical world stems from the equilibrium mechanisms in the human body. Suppose a person stands on a plate with sensors and suddenly this plate moves heavily. Then the person will try to stabilize and there are apparently two dominant mechanisms in our body to do this, and these are in different frequency bands: one reaction will be faster than the other. The goal is, given meaured (noisy) pressure data from one experiment for one person, to try to find which mechanism was dominant. Of course data from only one experiment will probably be not reliable enough so we would need multiple experiments. However, we can test wavelet techniques anyway.
	
	\vspace{2mm}
	\noindent
	Wavelets are an ideal mathematical technique to study this behaviour due to its inherent time-frequency localization. We are only interested in data after the plate moved heavily (after about 20 seconds in this case) and only in lower bands of the frequency spectrum since humans do not move that fast compared to the sampling frequency $f_s = \SI{3}{\kilo\hertz}$.
	
	\vspace{2mm}
	\noindent
	Another remark is that we will need a full wavelet decomposition of the signal because our interest is mainly in the low frequency domain and a wavelet starts with decomposing in the highest frequencies (iteratively dividing the spectrum in half). The data consists of roughly \num{120000} data points so there are $J = 16$ levels in total in the wavelet decomposition.
	
	\vspace{2mm}
	\noindent
	Let's first look at the data in Figure \ref{fig:pressure}. There is a clear peak or large variation in the data after 20 seconds. This is the sudden change in position of the plate and the human subject tries to stabilize him- or herself in the following seconds. The goal of the wavelet analysis is to find which frequencies contain much energy (these are `active') in the time interval [$\SI{20}{\second}, \SI{35}{\second}$]. Note that also the second movement of plate is visible around $\SI{37}{\second}$.
	
	\vspace{2mm}
	\noindent
	The scaling and wavelet coefficients of the wavelet decomposition of the whole signal are shown in Figure \ref{fig:pressurewaveletcoefficients}. We immediately see that coefficients are also very noisy for higher frequencies but the amplitude increases as the frequency lowers. This is exactly what is necessary since the person's reaction will be rather slow.
	\begin{figure}
		\centering
		\includegraphics[width=0.7\linewidth]{plots/pressure}
		\caption{The measured pressure signal as a function of time. The signal is quite noisy.}
		\label{fig:pressure}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=0.7\linewidth]{plots/pressurewaveletcoefficients}
		\caption{The scaling and wavelet coefficients of the pressure signal with their frequency range indicated on top.}
		\label{fig:pressurewaveletcoefficients}
	\end{figure}
	The wavelet transform in Figure \ref{fig:pressurewaveletcoefficients} is the transformation of the whole signal [$\SI{0}{\second}, \SI{40}{\second}$] but we are only interested in coefficients of wavelets/scaling functions that overlap with [$\SI{20}{\second}, \SI{35}{\second}$]. The energy of these coefficients is shown in Figure \ref{fig:waveletenergycoefficients}. The first peak in the signal corresponding to frequencies [$\SI{0}{\hertz}, \SI{0.025}{\hertz}$] are the scaling coefficients. These are not really of interest since they contain the DC-component of the pressure signal, so these coefficients will usually be systematically higher than the wavelets coefficients because the latter only encode differences. Then there are three more peaks in the coefficients. The first one for frequency interval [$\SI{0.05}{\hertz}, \SI{0.1}{\hertz}$]. This suggests movement in a timescale of about 10 seconds. This may be a bit too slow for human reaction and can possibly be attributed to the movement of the pressure scale itself. The other peaks around $0.38-\SI{0.75}{\hertz}$ can be human. It typically takes a few seconds to translate visual input to physical actions so movements on this timescale could be attributed to a stabilizing action based on the human visual system. Finally the third peak around $3-\SI{6}{\hertz}$ is a lot lower than the others but may be due to a different stabilizing mechanism in the human body but more experiments and data are needed to figure out if this is really a peak due to action based on the cochlear equilibrium sensors or just accumulated noise. One experiment is not enough to determine this. A final observation is the increase of the energy for very high frequencies. This is completely due to noise. If we look back at the coefficients in \ref{fig:pressurewaveletcoefficients} then the coefficients for high frequencies are all practically equal so these correspond to wavelets in the noise level. But because the number of coefficients doubles for every higher frequency interval (remember the finer time localization of the wavelets), we need to add double the amount of `equal' coefficients, meaning that the energy will artificially increase even if the data is just noise. This is not due to a sudden discovery of superhuman behaviour.
	
	\vspace{2mm}
	\noindent
	This noise only has a significant effect on the high frequency levels due to the approximate frequency localization of wavelets. Each time a low-pass filter is applied there will be a little bit of aliasing so that high frequency components are mapped to low frequencies but the effect is not big because like in Figure \ref{fig:filtersdb2} the edges of the low- and high-pass filters decrease rapidly outside their active domain. Applying a low-pass filter to the data as a preprocessing step will decrease this effect as the noise will be put the zero approximately for high frequencies.
	\begin{figure}
		\centering
		\includegraphics[width=0.9\linewidth]{plots/waveletenergycoefficients}
		\caption{Energy content of the wavelet and scaling coefficients per frequency interval.}
		\label{fig:waveletenergycoefficients}
	\end{figure}
	
	\newpage
	\appendix
	\section*{Appendix}
	This appendix includes a listing of the code.
	
	\subsection*{The frequency intervals using wavelet scales}
	
	\subsubsection*{\texttt{waveletbands}}
	
	\lstinputlisting{../code/waveletbands.m}
	
	\subsection*{Time localization}
	
	\subsubsection*{\texttt{evalscalingfunction}}	

	\lstinputlisting{../code/evalscalingfunction.m}
	
	\subsubsection*{\texttt{evalwavelet}}
	
	\lstinputlisting{../code/evalwavelet.m}
	
	\subsubsection*{\texttt{support}}
	
	\lstinputlisting{../code/support.m}
	
	\subsubsection*{\texttt{plotScalingFunctions}}
	
	\lstinputlisting{../code/plotscalingfunctions.m}
	
	\subsubsection*{\texttt{plotWaveletFunctions}}
	
	\lstinputlisting{../code/plotwaveletfunctions.m}		
	
	\subsection*{Frequency localization, revisited}
	
	\subsubsection*{\texttt{spectrum}}
	
	\lstinputlisting{../code/spectrum.m}
	
	\subsubsection*{\texttt{plotspectrum}}
	
	\lstinputlisting{../code/plotspectrum.m}
	
	\subsubsection*{\texttt{plotspectra}}
	
	\lstinputlisting{../code/plotspectra.m}	
	
	\subsubsection*{\texttt{fftSignal}}
	
	\lstinputlisting{../code/fftSignal.m}	
	
	\subsubsection*{\texttt{plotDFT}}
	
	\lstinputlisting{../code/plotDFT.m}	
	
	\subsection*{BOLD data analysis}
	
	\subsubsection*{\texttt{plotBold}}
	
	\lstinputlisting{../code/plotBold.m}
	
	\subsubsection*{\texttt{Main} of BOLD analysis}
	
	\lstinputlisting{../code/EX32.m}
	
	\subsection*{Pressure plate data}
	
	\subsubsection*{\texttt{Main} of the pressure analysis}	
	
	\lstinputlisting{../code/pressure_code.m}
\end{document}