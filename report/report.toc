\select@language {english}
\contentsline {section}{\numberline {1}Time- and frequency localization of wavelets using data}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}The frequency intervals of wavelet scales}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Time localization}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Frequency localization, revisited}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Analysis of two data sets}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Blood oxygen-level dependent imaging (BOLD)}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Human pressure response}{10}{subsection.2.2}
